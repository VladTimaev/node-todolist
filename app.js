
const express = require("express");
const bodyParser = require("body-parser");
const mongoose = require("mongoose");

// const date = require(__dirname + "/data.js");

const app = express();

mongoose.connect('mongodb://localhost:27017/todolistDB', {useNewUrlParser: true});

const itemsSchema = {
  name: String
};
const Item = mongoose.model("items", itemsSchema);

  const item1 = new Item ({
    name: "Welcome to your todolist!"
  });

  const item2 = new Item ({
    name: "Hit the + button to add a new item."
  });

  const item3 = new Item ({
    name: "<-- Hit this to delete an item."
  });

  const defaultItems = [item1,item2,item3];

 const listSchema = {
   name: String,
   items: [itemsSchema]
 }
const List = mongoose.model("List", listSchema);




app.set("view engine", "ejs");

app.use(bodyParser.urlencoded({extended:true}));
app.use(express.static("public"));
app.get("/", function(req, res){
  // // let day = date.getDate();
  Item.find({}, function(err, foundItems){
    res.render("list", {listTitle: "Today", newListItems: foundItems});
    if(foundItems === 0){
      Item.insertMany(defaultItems, function(err){
        if(err){
          console.log(err);
        } else{
          console.log('Successfully added new collection');
        }
      });
      res.redirect('/');
    } else{
      res.render("list", {listTitle: "Today", newListItems: foundItems});
    };
  });
});


  app.post("/", function(req, res){
  const itemName = req.body.newItem;

     const items = new Item ({
       name: itemName
     });
     items.save();
     res.redirect("/");
  });

 app.post("/delete", function(req, res){
   const checkedItemId = req.body.checkbox;

   Item.findByIdAndRemove(checkedItemId, function(err){
     if(!err){
      console.log('Successfully deleted...');
       res.redirect("/");
    } else{
      console.log("Errors")
    }
   });

 });
  app.get("/:customListName", function(req, res){
    const customListName = req.params.customListName;
     List.findOne({name: customListName}, function(err, foundList){
       if(!err){
         if(!foundList){
           const list = new List ({
             name: customListName,
             items: defaultItems
           });
          list.save();
          res.redirect("/" + customListName);
         } else{
           res.render("list", {listTitle: foundList.name, newListItems: foundList.items});
         };
       }
     });
  });
  // app.post("/work", function(request, response){
  //   let item = req.body.newItem;
  //   workItems.push(item);
  //   res.redirect("/work");
  //   console.log(req.body);
  // });
  app.get("/about", function(req, res){
    res.render("about");
  });

app.listen(8000, function(){
  console.log("Server start working in 3000 port...")
});
